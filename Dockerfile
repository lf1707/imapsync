## Dockerfile for building a docker imapsync image

# $Id: Dockerfile,v 1.882

FROM debian:stretch 

#LABEL maintainer "gilles.lamiral@laposte.net"
LABEL maintainer "lf1707 copy from gilles.lamiral"

RUN apt-get update \
  && apt-get install -y \
  wget \
  libauthen-ntlm-perl    \
  libclass-load-perl     \
  libcrypt-ssleay-perl   \
  libdata-uniqid-perl    \
  libdigest-hmac-perl    \
  libdist-checkconflicts-perl \
  libfile-copy-recursive-perl \
  libio-compress-perl     \
  libio-socket-inet6-perl \
  libio-socket-ssl-perl   \
  libio-tee-perl          \
  libmail-imapclient-perl \
  libmodule-scandeps-perl \
  libnet-ssleay-perl      \
  libpar-packer-perl      \
  libreadonly-perl        \
  libregexp-common-perl   \
  libsys-meminfo-perl     \
  libterm-readkey-perl    \
  libtest-fatal-perl      \
  libtest-mock-guard-perl \
  libtest-pod-perl        \
  libtest-requires-perl   \
  libtest-simple-perl     \
  libunicode-string-perl  \
  liburi-perl             \
  libtest-nowarnings-perl \
  libtest-deep-perl       \
  libtest-warn-perl       \
  make                    \
  cpanminus               \
  && cpanm Mail::IMAPClient \
  && cpanm JSON::WebToken   \
  && rm -rf /var/lib/apt/lists/*

RUN set -xe && \
    wget -q -O /usr/bin/imapsync.pl https://imapsync.lamiral.info/dist/imapsync && \
    chmod +x /usr/bin/imapsync.pl

ENTRYPOINT ["/usr/bin/imapsync.pl"]

# 
# End of imapsync Dockerfile 