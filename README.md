# imapsync
![logo_imapsync](/uploads/817643bf685b7f424e12fa01a34e2c1e/logo_imapsync.png)
    What is imapsync? (back to menu)
Imapsync is an IMAP transfers tool. The purpose of imapsync is to migrate IMAP accounts or to backup IMAP accounts. IMAP is one of the three current standard protocols to access mailboxes, the two other are POP3 and HTTP with webmails, webmails are often tied to an IMAP server.

Latest imapsync published release 1.882 was written on Saturday, 05-May-2018 23:10:47 CEST

Imapsync software is a command line tool that allows incremental and recursive IMAP transfers from one mailbox to another, both anywhere on the internet or in your local network. Imapsync runs on Windows, Linux, Mac OS X. "Incremental" means you can stop the transfer at any time and restart it later efficiently, without generating duplicates. "Recursive" means the complete folders hierarchy can be copied, all folders, all subfolders etc. "Command line" means it's not a graphical tool, on Windows you have to run imapsync from a batch file. Anyway there is a visual online service, you can try imapsync at https://i005.lamiral.info/X/

Imapsync can't migrate Contacts and Calendars. Most email systems don't set or get Contacts or Calendars via the IMAP protocol. No way via IMAP, no way via imapsync but it can be done with other tools or via export/import of csv or ics files. Also consider using caldavsynchronizer or asking experts at Sumatra company.

imapsync is not suitable for maintaining a synchronization between two active imap accounts while the user is working on both sides. Use offlineimap (written by John Goerzen) or mbsync (written by Michael R. Elkins) for bidirectionnal (2 ways) synchronizations.

Alternatives to imapsync are listed in the Similar softwares section.
# 重要特性
- 支持76种mailserver，https://imapsync.lamiral.info/S/imapservers.shtml
    
# imapsync不能迁移什么
- caldav/carddav ，imapsync不包括caldav协议，所以无法迁移联系人、群组、日历、任务等内容
- 双活账号的同步，请使用[offlineimap](http://www.offlineimap.org/), 或者[mbsync](http://isync.sourceforge.net/)
    
# 作者
    Gilles LAMIRAL 
    gilles@lamiral.info
    work cel: +33 6 19 22 03 54
    work desk: +33 9 51 84 42 42
    Postal address:
    22 La Billais
    BAULON 35580
    FRANCE
# 官网
    https://imapsync.lamiral.info/
# 类似的其他软件
    imapsync: https://github.com/imapsync/imapsync (this is an imapsync copy, sometimes delayed)
    imap_tools: http://www.athensfbc.com/imap_tools/. The imap_tools code is now at https://github.com/andrewnimmo/rick-sanders-imap-tools
    imaputils: http://code.google.com/p/imaputils/ (very old imap_tools fork)
    Doveadm-Sync: http://wiki2.dovecot.org/Tools/Doveadm/Sync ( Dovecot sync tool )
    davmail: http://davmail.sourceforge.net/
    offlineimap: http://offlineimap.org/
    mbsync: http://isync.sourceforge.net/
    mailsync: http://mailsync.sourceforge.net/
    mailutil: http://www.washington.edu/imap/ part of the UW IMAP tookit.
    imaprepl: https://bl0rg.net/software/ http://freecode.com/projects/imap-repl/
    imapcopy (Pascal): http://www.ardiehl.de/imapcopy/
    imapcopy (Java): https://code.google.com/p/imapcopy/
    imapsize: http://www.broobles.com/imapsize/
    migrationtool: http://sourceforge.net/projects/migrationtool/
    imapmigrate: http://sourceforge.net/projects/cyrus-utils/
    larch: https://github.com/rgrove/larch (derived from wonko_imapsync, good at Gmail)
    wonko_imapsync: http://wonko.com/article/554 (superseded by larch)
    pop2imap: http://www.linux-france.org/prj/pop2imap/
    exchange-away: http://exchange-away.sourceforge.net/
    SyncBackPro http://www.2brightsparks.com/syncback/sbpro.html
    ImapSyncClient https://github.com/ridaamirini/ImapSyncClient

# 关于镜像lf1707/imapsync
    参考下述后改写，详见本项目dockerfile
    - https://imapsync.lamiral.info/INSTALL.d/INSTALL.Ubuntu.txt
    - https://github.com/imapsync/imapsync
    
    本镜像现有2个版本：1.887和1.882.
    发行版为1.882，使用tag：latest
    最新版为1.887，使用tag：1.887
    
# 安装
    git clone ssh://git@gitlab.expresscare.cn:10022/linfang/imapsync.git
    chmod +x *.sh
    
# 使用

    1、单个账号的迁移
    单次命令,可以使用：
    docker run --name imapsync --rm -ti lf1707/imapsync \
     --host1 {server1.imap.tld} --user1 {boite@email.tld} --password1 {motdepasse1} \
     --host2 {server2.imap.tld} --user2 {boite@email.tld} --password2 {motdepasse2}
     
     也可使用sh：./imapsync.sh
     ./imapsync.sh $host1 $user1 $pass1 $host2 $user2 $pass2
     
    2、多个账号的迁移
    2.1 编辑imap-file.txt,安装里面提示的格式输入需要迁移的账号
    2.2 执行
        ./multi-imapsync.sh
        
    3、官网的在线迁移工具
    https://i005.lamiral.info/X/